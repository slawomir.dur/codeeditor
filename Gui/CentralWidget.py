from PyQt5.QtWidgets import QWidget, QSplitter, QPushButton, QHBoxLayout
from PyQt5.QtCore import Qt
from .ProjectView import ProjectView
from .CodeEditor import CodeEditor
from PyQt5.QtGui import QScreen
from PyQt5.QtCore import QSize


class CentralWidget(QWidget):
    def __init__(self, screen: QScreen):
        super(CentralWidget, self).__init__()
        splitter = QSplitter()
        splitter.setOrientation(Qt.Horizontal)
        splitter.addWidget(ProjectView())
        splitter.addWidget(CodeEditor())
        sizes = [screen.size().width()*0.15, screen.size().width()*0.85]
        splitter.setSizes(sizes)

        primeLayout = QHBoxLayout()
        primeLayout.addWidget(splitter)
        self.setLayout(primeLayout)
