from PyQt5.QtWidgets import QMainWindow, QStatusBar
from PyQt5.QtGui import QScreen
from .MenuBar import MenuBar
from .CentralWidget import CentralWidget


class MainWindow(QMainWindow):
    def __init__(self, screen: QScreen):
        super(MainWindow, self).__init__()
        self.setMinimumSize(screen.availableSize().width(), screen.availableSize().height())
        self.set_menu()
        self.set_status_bar()
        self.set_central_widget(screen)

    def set_menu(self):
        menuBar = MenuBar()
        self.setMenuBar(menuBar)

    def set_status_bar(self):
        statusBar = QStatusBar()
        statusBar.showMessage('Opened successfully')
        self.setStatusBar(statusBar)

    def set_central_widget(self, screen: QScreen):
        central_widget = CentralWidget(screen)
        self.setCentralWidget(central_widget)



