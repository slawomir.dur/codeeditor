from PyQt5.QtWidgets import QMenuBar, QMenu, QAction
from PyQt5.QtCore import QCoreApplication


class MenuBar(QMenuBar):
    _fileMenu = None
    _editMenu = None
    _viewMenu = None
    _runMenu = None
    _toolsMenu = None
    _helpMenu = None

    def __init__(self):
        super(MenuBar, self).__init__()
        self._fileMenu = QMenu(QCoreApplication.translate('MenuBar', 'File'))
        self._editMenu = QMenu(QCoreApplication.translate('MenuBar', 'Edit'))
        self._viewMenu = QMenu(QCoreApplication.translate('MenuBar', 'View'))
        self._runMenu = QMenu(QCoreApplication.translate('MenuBar', 'Run'))
        self._toolsMenu = QMenu(QCoreApplication.translate('MenuBar', 'Tools'))
        self._helpMenu = QMenu(QCoreApplication.translate('MenuBar', 'Help'))

        menus = [self._fileMenu,self._editMenu,self._viewMenu,self._runMenu,self._toolsMenu,self._helpMenu]
        for menu in menus:
            self.addMenu(menu)
